/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author LuizGM
 */
public class Pagamento {
    private HashMap<Integer, Double> comissao = new HashMap<>();

    public Pagamento() {
    }
    
    private final double VALORCOMISSAO = 0.05;
    
    public void calcComissao(List<Vendas> lista){
        for(Vendas v : lista){
            
            int codigo = v.getVendedor().getCodigo() ; 
            
            if(!comissao.containsKey(codigo)){
                comissao.put(codigo,v.getTotal() * VALORCOMISSAO) ; 
            }else{
               double comissaoParcial = comissao.get(codigo).doubleValue() ; 
               comissaoParcial+= (v.getTotal() * VALORCOMISSAO) ; 
               comissao.put(codigo, comissaoParcial);
            }
            
            
        }
        
        
        
        
    }
    
    public double getComissaoVendedor(Vendedor vendedor){
        return this.comissao.get(vendedor.getCodigo()).doubleValue() ; 
    }

            
}

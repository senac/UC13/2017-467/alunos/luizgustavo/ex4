/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Produto;
import br.com.senac.Vendas;
import br.com.senac.Vendedor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LuizGM
 */
public class TestVenda {
    
    public TestVenda() {
    }
    
    @Test
    public void totalNota(){
        
        Vendas vendas = new Vendas(new Vendedor(1, "Marcos"));
        Produto produto = new Produto(1, "Agua");
        vendas.adicionarItem(produto , 1 , 10 ) ; 
        
        double resultado = vendas.getTotal() ; 
        
        
        assertEquals(resultado, 10 , 0.05);
        
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Pagamento;
import br.com.senac.Produto;
import br.com.senac.Vendas;
import br.com.senac.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LuizGM
 */
public class FolhaTeste {
    
    public FolhaTeste() {
    }
    
    @Test
    public void deveCalcularComissaoParaMarcos(){
        
        Vendedor vendedor = new Vendedor(1, "Marcos");
        Produto produto = new Produto(1, "Agua");
        
        
        Vendas venda1 = new Vendas(vendedor) ;
        venda1.adicionarItem(produto , 1 , 10 ) ;
        
        Vendas venda2 = new Vendas(vendedor);
        venda2.adicionarItem(produto, 1, 10);
        
        List<Vendas> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        Pagamento folhaPagamento = new Pagamento(); 
        folhaPagamento.calcComissao(listaVendas) ; 
        double comissao = folhaPagamento.getComissaoVendedor(vendedor);
        
        assertEquals(1, comissao , 0.01);
        
        
        
        
        
        
    }
    
    
    @Test
    public void deveCalcularComissaoParaMarcosEFelipe(){
        
        Vendedor marcos = new Vendedor(1, "Marcos");
        Vendedor felipe = new Vendedor(2, "Felipe");
        
        Produto produto = new Produto(1, "Sabao");
        
        
        Vendas venda1 = new Vendas(marcos) ;
        venda1.adicionarItem(produto , 10 , 10 ) ;
        
        Vendas venda2 = new Vendas(felipe);
        venda2.adicionarItem(produto, 10, 20);
        
        List<Vendas> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        Pagamento folhaPagamento = new Pagamento(); 
        folhaPagamento.calcComissao(listaVendas) ; 
        double comissaoMarcos = folhaPagamento.getComissaoVendedor(marcos) ; 
        
        assertEquals(5.0, comissaoMarcos , 0.01);
        
        
        
        
        
        
    }

}
